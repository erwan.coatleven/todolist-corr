import { Injectable } from '@angular/core';
import { Task } from '../models/task';

@Injectable({
  providedIn: 'root'
})
export class TaskService {
  set tasks(tasks: Task[]) {
    localStorage.setItem('tasks', JSON.stringify(tasks));
  }

  get tasks(): Task[] {
    return JSON.parse(localStorage.getItem('tasks') || '[]');
  }

  constructor() { }

  // getTasks() {
  //   return JSON.parse(localStorage.getItem('tasks') || '[]');
  // }

  // saveTasks(tasks: Task[]) {
  //   localStorage.setItem('tasks', JSON.stringify(tasks));
  // }
}
